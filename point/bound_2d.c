/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bound_2d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/02 18:48:49 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <point.h>

void	update_bound_2d(t_bound_2d *b, t_point_2d p)
{
	if (p.x < b->min.x)
		b->min.x = p.x;
	if (p.x > b->max.x)
		b->max.x = p.x;
	if (p.y < b->min.y)
		b->min.y = p.y;
	if (p.y > b->max.y)
		b->max.y = p.y;
}

t_point_2d	center_2d(t_bound_2d b)
{
	t_point_2d	p;
	t_point_2d	s;

	s = size_2d(b);
	p.x = b.min.x + s.x / 2;
	p.y = b.min.y + s.y / 2;
	return (p);
}

t_bound_2d	empty_bound_2d(void)
{
	t_bound_2d	b;

	b.min = empty_point_2d();
	b.max = empty_point_2d();
	return (b);
}

t_bound_2d	scale_bound_2d(t_bound_2d b1, float f)
{
	t_bound_2d	b2;

	b2.min = scale_point_2d(b1.min, f);
	b2.max = scale_point_2d(b1.max, f);
	return (b2);
}

int	in_bound_floor_2d(t_bound_2d b, t_point_2d p)
{
	return (((p.x >= floor(b.min.x) && p.x < floor(b.max.x) + 1)
			|| (p.x < floor(b.min.x) + 1 && p.x >= floor(b.max.x)))
		&& ((p.y >= floor(b.min.y) && p.y < floor(b.max.y) + 1)
			|| (p.y < floor(b.min.y) + 1 && p.y >= floor(b.max.y))));
}

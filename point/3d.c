/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   3d.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 05:37:30 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <point.h>
#include <stdio.h>

void	update_bound_3d(t_bound_3d *b, t_point_3d p)
{
	if (p.x < b->min.x)
		b->min.x = p.x;
	if (p.x > b->max.x)
		b->max.x = p.x;
	if (p.y < b->min.y)
		b->min.y = p.y;
	if (p.y > b->max.y)
		b->max.y = p.y;
	if (p.z < b->min.z)
		b->min.z = p.z;
	if (p.z > b->max.z)
		b->max.z = p.z;
}

t_point_3d	size_3d(t_bound_3d b)
{
	t_point_3d	p;

	p.x = b.max.x - b.min.x;
	p.y = b.max.y - b.min.y;
	p.z = b.max.z - b.min.z;
	return (p);
}

t_point_3d	center_3d(t_bound_3d b)
{
	t_point_3d	p;
	t_point_3d	s;

	s = size_3d(b);
	p.x = b.min.x + (s.x / 2);
	p.y = b.min.y + (s.y / 2);
	p.z = b.min.z + (s.z / 2);
	return (p);
}

t_point_3d	empty_point_3d(void)
{
	t_point_3d	p;

	p.x = 0;
	p.y = 0;
	p.z = 0;
	p.c = 0;
	return (p);
}

t_bound_3d	empty_bound_3d(void)
{
	t_bound_3d	b;

	b.min = empty_point_3d();
	b.max = empty_point_3d();
	return (b);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point_2d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/02 19:08:16 by racherom          #+#    #+#             */
/*   Updated: 2023/11/09 18:52:21 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <point.h>

t_point_2d	empty_point_2d(void)
{
	t_point_2d	p;

	p.x = 0;
	p.y = 0;
	return (p);
}

t_point_2d	scale_point_2d(t_point_2d p1, float f)
{
	t_point_2d	p2;

	p2.x = p1.x * f;
	p2.y = p1.y * f;
	return (p2);
}

t_point_2d	distance_point_2d(t_point_2d p1, t_point_2d p2)
{
	p2.x -= p1.x;
	p2.y -= p1.y;
	return (p2);
}

t_point_2d	size_2d(t_bound_2d b)
{
	t_point_2d	p;

	p.x = b.max.x - b.min.x;
	p.y = b.max.y - b.min.y;
	return (p);
}

float	distance_2d(t_point_2d p1, t_point_2d p2)
{
	return (sqrtf(powf(p2.x - p1.x, 2) + powf(p2.y - p1.y, 2)));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 02:48:36 by racherom          #+#    #+#             */
/*   Updated: 2023/11/09 19:00:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>

int	argb(unsigned char a, unsigned char r, unsigned char g, unsigned char b)
{
	return (a << 24 | r << 16 | g << 8 | b);
}

int	main(int argc, char **argv)
{
	t_app	app;

	if (app_init(&app))
		return (1);
	if (app_parse(&app, argc, argv))
		return (1);
	return (app_start(&app));
}

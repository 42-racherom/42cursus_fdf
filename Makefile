# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rauer <rauer@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/22 20:19:47 by rauer             #+#    #+#              #
#    Updated: 2023/11/13 22:52:03 by rauer            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
SRC = main.c
OBJ = $(SRC:.c=.o)
LIB = app fdf ft win isometric point m mlx
MLX_MACOS = minilibx_macos_sierra_20161017.tgz
ifeq ($(shell uname), Linux)
OS_FLAGS = -L/usr/lib -lXext -lX11 -lz
OS = linux
else
OS_FLAGS = -framework OpenGL -framework AppKit
OS = macos
endif
LIBS = mlx_$(OS)/libmlx.a ft/libft.a fdf_map/libfdf.a win/libwin.a \
	isometric/libisometric.a point/libpoint.a app/libapp.a
LIB_DIRS = $(patsubst %/,%,$(dir $(LIBS)))
LIB_FLAGS = $(addprefix -L, $(LIB_DIRS)) $(addprefix -l, $(LIB)) $(addprefix -I, $(LIB_DIRS))
GCC = gcc -Wall -Wextra -Werror

all: mlx_$(OS) $(LIBS) $(NAME)

blah:
	echo $(LIB_DIRS)

$(NAME): mlx_$(OS)/libmlx.a $(SRC) $(LIBS)
	$(GCC) $(SRC) $(DEBUG) $(LIB_FLAGS) $(OS_FLAGS) -o $@

.PHONY: all clean fclean

mlx_macos/libmlx.a:: mlx_macos

mlx_linux/libmlx.a:: mlx_linux FORCE
	git -C mlx_linux pull

$(LIBS):: FORCE
	@$(MAKE) -C $(dir $@) DEBUG="$(DEBUG)"

mlx_linux:
	git clone --depth=1 https://github.com/42Paris/minilibx-linux.git mlx_linux

$(MLX_MACOS):
	wget https://cdn.intra.42.fr/document/document/20619/minilibx_macos_sierra_20161017.tgz

mlx_macos: $(if $(wildcard mlx_macos),,$(MLX_MACOS))
	tar -xzf $<
	mv minilibx_macos $@

FORCE: ;

%.o: %.c
	$(GCC) $(DEBUG) $(addprefix -I, $(LIB)) -c $< -o $@

clean_%:
	-$(MAKE) -iC $* clean

clean: $(addprefix clean_, $(LIB_DIRS))
	rm -rf $(OBJ) $(MLX_MACOS)

mlx_clean:
	rm -rf mlx* minilibx*

fclean_%:
	-$(MAKE) -ikC $* fclean

fclean: clean $(addprefix fclean_, $(LIB_DIRS))
	rm -rf $(NAME)

re: fclean all

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isometric.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/20 01:18:47 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 02:41:59 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <isometric.h>
#include <math.h>
#include <point.h>

t_point_2d	isometric_project(t_point_3d p3d, t_point_2d o, float zoom)
{
	o.c = p3d.c;
	o.x += (p3d.x - p3d.y) * zoom / 2;
	o.y += (p3d.x / 2 + p3d.y / 2 - p3d.z) * zoom * tan(M_PI / 6);
	return (o);
}

t_point_3d	isometric_translate(t_point_2d p2d, t_point_2d o, float zoom)
{
	float		x;
	float		y;
	t_point_3d	p3d;

	y = (p2d.y - o.y) / (zoom * tan(M_PI / 6) / 2);
	x = (p2d.x - o.x) / (zoom / 2);
	p3d.x = (y + x) / 2;
	p3d.y = (y - x) / 2;
	p3d.z = 0;
	p3d.c = p2d.c;
	return (p3d);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isometric.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/20 01:23:17 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:57:04 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ISOMETRIC_H
# define ISOMETRIC_H
# include <point.h>

typedef struct s_isometric
{
	t_bound_2d	b2d;
	t_bound_3d	b3d;
}				t_isometric;

t_point_2d		isometric_project(t_point_3d p3d, t_point_2d o, float zoom);
t_point_3d		isometric_translate(t_point_2d p2d, t_point_2d o, float zoom);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_parse.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/02 03:52:05 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 04:07:01 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>
#include <ft.h>
#include <point.h>
#include <unistd.h>

int	extend(void **a, size_t s, int l, int e)
{
	void	*aa;

	aa = ft_calloc(s, l + e);
	if (!aa)
		return (1);
	ft_memcpy(aa, *a, s * l);
	if (*a)
		free(*a);
	*a = aa;
	return (0);
}

int	fdf_extend(t_fdf *fdf)
{
	int	l;

	if (fdf->cap > fdf->len)
		return (0);
	l = 10;
	if (fdf->w > 0)
		l = fdf->w - (fdf->cap % fdf->w);
	if (extend((void *)&(fdf->a), sizeof(t_point_3d), fdf->cap, l))
		return (1);
	fdf->cap += l;
	return (0);
}

int	fdf_parse_field(char *field, int *color)
{
	int	height;
	int	m;

	height = 0;
	m = 1;
	if (*field == '+')
		field++;
	else if (*field == '-')
	{
		m = -1;
		field++;
	}
	while (*field >= '0' && *field <= '9')
		height = (height * 10) + (m * (*(field++) - '0'));
	if (*field == ',')
		*color = ft_hex_atoi(++field);
	return (height);
}

int	fdf_parse_row(t_fdf *fdf, char *line)
{
	t_point_3d	p;
	char		**row;
	int			i;

	p.y = (fdf->h++) * fdf->dist;
	i = 0;
	row = ft_split_free(ft_strtrim(line, " \n"), ' ');
	while ((fdf->w == 0 || i < fdf->w) && row[i])
	{
		fdf_extend(fdf);
		p.x = i * fdf->dist;
		p.c = fdf->color;
		p.z = fdf_parse_field(row[i], &(p.c));
		free(row[i++]);
		update_bound_3d(&fdf->b, p);
		fdf->a[fdf->len++] = p;
	}
	fdf->w = i;
	free(row);
	return (0);
}

int	fdf_parse(t_fdf *fdf, int fd)
{
	char	*line;

	line = ft_gnl(fd);
	while (line)
	{
		if (fdf_parse_row(fdf, line))
			return (1);
		free(line);
		line = ft_gnl(fd);
	}
	fdf->c = center_3d(fdf->b);
	return (0);
}

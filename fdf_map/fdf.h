/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/02 03:49:48 by racherom          #+#    #+#             */
/*   Updated: 2023/11/09 18:56:29 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include <ft.h>
# include <point.h>
# include <unistd.h>

typedef struct s_fdf
{
	t_point_3d	*a;
	int			w;
	int			h;
	int			len;
	int			cap;
	int			dist;
	int			color;
	int			bg_color;
	t_bound_3d	b;
	t_point_3d	c;
}				t_fdf;

int				fdf_parse(t_fdf *fdf, int fd);
int				fdf_init(t_fdf *fdf);

#endif

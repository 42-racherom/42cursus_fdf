/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/02 05:19:13 by racherom          #+#    #+#             */
/*   Updated: 2023/11/09 21:59:22 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>
#include <point.h>

int	fdf_init(t_fdf *fdf)
{
	fdf->a = 0;
	fdf->w = 0;
	fdf->h = 0;
	fdf->len = 0;
	fdf->cap = 0;
	fdf->dist = 12;
	fdf->bg_color = 0;
	fdf->color = 0xFFFFFF;
	fdf->b = empty_bound_3d();
	return (0);
}

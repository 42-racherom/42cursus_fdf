/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags_puthex.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/22 19:50:59 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:35:45 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_hexlen(unsigned long i, int precision, int prefix)
{
	int	len;

	len = 0;
	while (i)
	{
		i >>= 4;
		len++;
	}
	if (len < precision)
		len = precision;
	if (prefix)
		len += 2;
	return (len);
}

static int	ft_put_hex_prefix(int fd, char h)
{
	int	len;

	len = ft_putchar(fd, '0');
	len += ft_putchar(fd, h + 23);
	return (len);
}

static int	ft_puthex(int fd, unsigned long i, int precision, char h)
{
	char	c;
	int		len;

	len = 0;
	if (precision > 1 || i > 15)
		len = ft_puthex(fd, i >> 4, precision - 1, h);
	if (precision <= 0 && i == 0)
		return (0);
	c = i & 15;
	if (c > 9)
		c += h - 10;
	else
		c += '0';
	return (len + ft_putchar(fd, c));
}

int	ft_flags_puthex(int fd, unsigned long i, t_flags flags, char h)
{
	int	len;
	int	space;

	len = ft_hexlen(i, flags.precision, flags.flags & 4);
	space = 0;
	if (flags.width > len)
		space = flags.width - len;
	len = 0;
	if (!(flags.flags & 2) && !(flags.flags & 1) && space > 0)
		ft_fill(fd, ' ', space);
	if (flags.flags & 4)
		len = ft_put_hex_prefix(fd, h);
	if (flags.flags & 2 && !(flags.flags & 1) && space > 0)
		ft_fill(fd, '0', space);
	len += ft_puthex(fd, i, flags.precision, h);
	ft_flags_after(fd, flags, len);
	return (space + len);
}

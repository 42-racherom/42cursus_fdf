/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/17 19:12:25 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>

static int	ft_is_whitespace(char c)
{
	return ((c >= 9 && c <= 13) || c == ' ');
}

static int	ft_is_hex(char c)
{
	return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A'
			&& c <= 'F'));
}

unsigned int	ft_hex_atoi(const char *str)
{
	unsigned int	i;

	if (!str)
		return (0);
	i = 0;
	if (*str == '#')
		str++;
	else if (str[0] == '0' && str[1] == 'x')
		str += 2;
	while (ft_is_hex(*str))
	{
		i <<= 4;
		if (ft_isdigit(*str))
			i += *str - '0';
		else if (ft_isupper(*str))
			i += *str - 31;
		else
			i += *str - 51;
		str++;
	}
	return (i);
}

int	ft_atoi(const char *str)
{
	int	i;
	int	m;

	if (!str)
		return (0);
	i = 0;
	m = 1;
	while (ft_is_whitespace(*str))
		str++;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		m = -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
		i = (i * 10) + (m * (*(str++) - '0'));
	return (i);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_flags.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 21:40:06 by rauer             #+#    #+#             */
/*   Updated: 2023/01/25 01:50:16 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf.h"

static t_flags	ft_new_flags(void)
{
	t_flags	f;

	f.flags = 0;
	f.width = 0;
	f.offset = 0;
	f.precision = 1;
	return (f);
}

static int	ft_parse_num_flag(char const *flags, int *num, va_list *ptr)
{
	int	i;

	if (*flags == '*')
	{
		*num = va_arg(*ptr, int);
		return (1);
	}
	i = 0;
	*num = 0;
	while (flags[i] >= '0' && flags[i] <= '9')
		*num = (*num * 10) + (flags[i++] - '0');
	return (i);
}

static int	ft_parse_flag(char const *flags, t_flags *f, va_list *ptr)
{
	if (*flags == '-')
		f->flags |= 1;
	else if (*flags == '0')
		f->flags |= 2;
	else if (*flags > '0' && *flags <= '9')
		return (ft_parse_num_flag(flags, &f->width, ptr));
	else if (*flags == '#')
		f->flags |= 4;
	else if (*flags == ' ')
		f->flags |= 8;
	else if (*flags == '+')
		f->flags |= 16;
	else if (*flags == '.')
	{
		f->flags |= 32;
		return (ft_parse_num_flag(flags + 1, &f->precision, ptr) + 1);
	}
	else
		return (0);
	return (1);
}

t_flags	ft_parse_flags(char const *flags, va_list *ptr)
{
	t_flags	f;
	int		offset;

	f = ft_new_flags();
	offset = 0;
	while (1)
	{
		offset = ft_parse_flag(flags + f.offset, &f, ptr);
		if (!offset)
		{
			if (f.flags & 32)
				f.flags &= ~2;
			return (f);
		}
		f.offset += offset;
	}
}

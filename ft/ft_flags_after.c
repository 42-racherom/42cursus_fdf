/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags_after.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/22 01:53:28 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:31:27 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_flags_after(int fd, t_flags flags, int len)
{
	if (flags.flags & 1 && flags.width > len)
		return (ft_fill(fd, ' ', flags.width - len));
	return (0);
}

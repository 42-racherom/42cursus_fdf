/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/18 16:55:41 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>
#include <stdlib.h>

char	*ft_strdup(const char *s1)
{
	size_t	l;
	size_t	i;
	char	*s2;

	l = 0;
	while (s1[l++])
		;
	s2 = ft_calloc(l, sizeof(char));
	if (!s2)
		return (NULL);
	i = 0;
	while (i < l)
		s2[i++] = *(s1++);
	return (s2);
}

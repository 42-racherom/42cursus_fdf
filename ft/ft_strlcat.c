/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/17 14:23:43 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>
#include <stdlib.h>

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t	i;
	size_t	l;
	size_t	d;

	i = 0;
	while (i < dstsize && dst[i])
		i++;
	l = i;
	while (*src && i + 1 < dstsize)
		dst[i++] = *(src++);
	d = i;
	while (*(src++))
		i++;
	if (l >= dstsize)
		return (i);
	if (dstsize > d)
		dst[d] = 0;
	return (i);
}

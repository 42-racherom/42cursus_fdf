/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 04:14:25 by rauer             #+#    #+#             */
/*   Updated: 2023/06/23 19:00:12 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>

int	ft_fprintf_va(int fd, const char *format, va_list *ptr)
{
	int	len;
	int	i;

	if (!format)
		return (0);
	i = 0;
	while (format[i])
	{
		if (format[i] != '%')
			len += ft_putchar(fd, format[i++]);
		else
		{
			i++;
			i += ft_conversions(fd, ptr, &len, format + i);
		}
	}
	return (len);
}

int	ft_printf(const char *format, ...)
{
	va_list	ptr;
	int		len;

	if (!format)
		return (0);
	va_start(ptr, format);
	len = ft_fprintf_va(1, format, &ptr);
	va_end(ptr);
	return (len);
}

int	ft_println(const char *line)
{
	int	len;

	len = 0;
	while (line[len])
		len++;
	write(1, line, len);
	write(1, "\n", 1);
	return (len + 1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_s.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 04:58:38 by rauer             #+#    #+#             */
/*   Updated: 2023/06/13 23:18:06 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>
#include <stdio.h>

int	ft_convert_s(int fd, va_list *ptr, t_flags flags)
{
	char	*str;
	int		len;
	int		space;

	str = va_arg(*ptr, char *);
	if (!str)
		str = "(null)";
	len = 0;
	while (str[len] && (!(flags.flags & 32) || flags.precision > len))
		len++;
	space = ft_flags_befor(fd, flags, len);
	write(fd, str, len);
	space += ft_flags_after(fd, flags, len);
	return (space + len);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/16 18:40:41 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>
#include <stdlib.h>

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	char	*src_str;
	char	*dst_str;

	if (src == dst)
		return (dst);
	if (src > dst)
		return (ft_memcpy(dst, src, n));
	src_str = (char *)src;
	dst_str = (char *)dst;
	while (n-- > 0)
		dst_str[n] = src_str[n];
	return (dst);
}

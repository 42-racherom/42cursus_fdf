/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/18 20:44:25 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 04:08:10 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>

void	ft_fill(char **a, const char *s, char c)
{
	int	i;
	int	j;
	int	k;

	i = 0;
	j = 0;
	k = 0;
	while (s[i])
	{
		if (s[i] == c)
			i++;
		else if (s[i + j] == c || s[i + j] == 0)
		{
			a[k++] = ft_substr(s, i, j);
			i += j;
			j = 0;
		}
		else
			j++;
	}
}

char	**ft_split(const char *s, char c)
{
	int		i;
	int		k;
	char	**a;

	if (!s)
		return (0);
	i = 0;
	k = 0;
	while (s[i++])
	{
		if (s[i - 1] != c && (s[i] == c || s[i] == 0))
			k++;
	}
	a = ft_calloc(k + 1, sizeof(char *));
	if (!a)
		return (0);
	a[k] = 0;
	ft_fill(a, s, c);
	return (a);
}

char	**ft_split_free(char *s, char c)
{
	char	**r;

	if (!s)
		return (0);
	r = ft_split(s, c);
	free(s);
	return (r);
}

void	ft_fill_atoi(int *a, const char *s)
{
	int	i;

	i = 0;
	while (*s)
	{
		if (!(ft_isdigit(*s) || *s == '-'))
			s++;
		else
		{
			a[i++] = ft_atoi(s++);
			while (ft_isdigit(*s))
				s++;
		}
	}
}

int	*ft_split_atoi(const char *s, int *l)
{
	int	i;
	int	k;
	int	*a;

	if (!s || !*s)
		return (0);
	i = 0;
	k = 0;
	while (s[i++])
		if (ft_isdigit(s[i - 1]) && !ft_isdigit(s[i]))
			k++;
	if (k == 0)
		return (0);
	if (l)
		*l = -1;
	a = ft_calloc(k, sizeof(int));
	if (!a)
		return (0);
	ft_fill_atoi(a, s);
	if (l)
		*l = k;
	return (a);
}

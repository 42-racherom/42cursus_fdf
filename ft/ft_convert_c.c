/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_c.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 04:26:08 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:26:59 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdarg.h>
#include "ft_printf.h"

int	ft_convert_c(int fd, va_list *ptr, t_flags flags)
{
	int		space;
	char	c;

	(void)flags;
	c = va_arg(*ptr, int);
	space = ft_flags_befor(fd, flags, 1);
	ft_putchar(fd, (char)c);
	space += ft_flags_after(fd, flags, 1);
	return (++space);
}

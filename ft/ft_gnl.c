/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gnl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 21:00:56 by rauer             #+#    #+#             */
/*   Updated: 2023/06/23 21:04:58 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#ifndef BUFFER_SIZE
# define BUFFER_SIZE 1028
#endif

int	fill_buffer(int fd, char *b)
{
	int	size;

	size = read(fd, b, BUFFER_SIZE);
	if (size < 0)
		*b = 0;
	else if (size < BUFFER_SIZE)
		b[size] = 0;
	return (size);
}

void	coppy_str(char *dst, char *src, int i)
{
	while (i-- > 0)
		*(dst++) = *(src++);
	*dst = 0;
}

char	*append_buffer(char *str1, char *b, int i)
{
	int		j;
	char	*str2;

	j = 0;
	while (str1 && str1[j])
		j++;
	str2 = malloc(j + i + 1);
	if (!str2 && str1)
		free(str1);
	if (!str2)
		return (NULL);
	coppy_str(str2, str1, j);
	if (str1)
		free(str1);
	coppy_str(str2 + j, b, i);
	j = 0;
	while (i < BUFFER_SIZE && b[i])
		b[j++] = b[i++];
	b[j] = 0;
	return (str2);
}

char	*buffer_read_line(int fd, char *str1, char *b)
{
	char	*str2;
	int		i;

	i = 0;
	while (i < BUFFER_SIZE && b[i] && b[i] != '\n')
		i++;
	if (i < BUFFER_SIZE && b[i] == '\n')
		i++;
	if (!i)
		return (str1);
	str2 = append_buffer(str1, b, i);
	i = 0;
	while (str2[i] && str2[i] != '\n')
		i++;
	if (str2[i] != '\n' && !*b && fill_buffer(fd, b) > 0)
		str2 = buffer_read_line(fd, str2, b);
	return (str2);
}

char	*ft_gnl(int fd)
{
	static char	*buffer;
	char		*line;

	if (fd < 0)
		return (NULL);
	if (!buffer)
	{
		buffer = malloc(BUFFER_SIZE);
		if (!buffer)
			return (NULL);
		fill_buffer(fd, buffer);
	}
	line = buffer_read_line(fd, NULL, buffer);
	if (buffer && !*buffer)
	{
		free(buffer);
		buffer = NULL;
	}
	return (line);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/17 18:46:03 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>
#include <stdlib.h>

static int	is_str(const char *haystack, const char *needle)
{
	int	i;

	i = 0;
	while (needle[i])
	{
		if (haystack[i] != needle[i])
			return (0);
		i++;
	}
	return (1);
}

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	ln;
	size_t	lh;

	if (!needle[0] || !needle)
		return ((char *)haystack);
	if (!len)
		return (NULL);
	lh = ft_strlen(haystack);
	if (lh < len)
		len = lh;
	ln = ft_strlen(needle);
	if (len < ln)
		return (NULL);
	len -= ln;
	i = 0;
	while (haystack[i] && i <= len)
	{
		if (haystack[i] == needle[0] && is_str(haystack + i, needle))
			return ((char *)haystack + i);
		i++;
	}
	return (NULL);
}

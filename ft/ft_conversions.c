/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conversions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 04:45:13 by rauer             #+#    #+#             */
/*   Updated: 2023/06/23 19:00:10 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>
#include <stdio.h>

int	ft_conversions(int fd, va_list *ptr, int *len, const char *format)
{
	char	selector;
	t_flags	flags;

	flags = ft_parse_flags(format, ptr);
	format += flags.offset++;
	selector = *format;
	if (selector == 'c')
		*len += ft_convert_c(fd, ptr, flags);
	else if (selector == 's')
		*len += ft_convert_s(fd, ptr, flags);
	else if (selector == 'p')
		*len += ft_convert_p(fd, ptr, flags);
	else if (selector == 'd' || selector == 'i')
		*len += ft_convert_i(fd, ptr, flags);
	else if (selector == 'u')
		*len += ft_convert_u(fd, ptr, flags);
	else if (selector == 'x')
		*len += ft_convert_x(fd, ptr, flags);
	else if (selector == 'X')
		*len += ft_convert_xx(fd, ptr, flags);
	else if (selector == '%')
		*len += ft_putchar(fd, '%');
	return (flags.offset);
}

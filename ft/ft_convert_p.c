/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_p.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 21:12:22 by rauer             #+#    #+#             */
/*   Updated: 2023/06/14 17:51:01 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>

int	ft_convert_p(int fd, va_list *ptr, t_flags flags)
{
	void	*i;

	i = va_arg(*ptr, void *);
	flags.flags |= 4;
	return (ft_flags_puthex(fd, (unsigned long)i, flags, 'a'));
}

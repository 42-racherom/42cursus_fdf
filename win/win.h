/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 01:25:38 by rauer             #+#    #+#             */
/*   Updated: 2023/11/09 18:39:40 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WIN_H
# define WIN_H
# include <mlx.h>
# include <point.h>

typedef struct s_win
{
	void	*mlx;
	void	*img;
	int		w;
	int		h;
	int		bpp;
	int		ll;
	int		endian;
	void	*addr;
	void	*win;

}			t_win;
int			win_pixel_put(t_win *win, t_point_2d p);
void		win_draw_line(t_win *win, t_bound_2d b);
void		win_background(t_win *win, int color);
int			win_init(t_win *win);
void		win_flush(t_win *win);
void		win_close(t_win *win);

#endif

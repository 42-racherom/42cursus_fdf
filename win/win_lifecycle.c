/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_lifecircle.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/09 22:15:44 by racherom          #+#    #+#             */
/*   Updated: 2023/11/09 22:17:27 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <win.h>

int	win_init(t_win *win)
{
	win->mlx = mlx_init();
	if (!win->mlx)
		return (1);
	win->img = mlx_new_image(win->mlx, win->w, win->h);
	if (!win->img)
		return (1);
	win->addr = mlx_get_data_addr(win->img, &win->bpp, &win->ll, &win->endian);
	if (!win->addr)
	{
		mlx_destroy_image(win->mlx, win->img);
		return (1);
	}
	win->win = mlx_new_window(win->mlx, win->w, win->h, "Wireframe");
	if (!win->win)
	{
		mlx_destroy_image(win->mlx, win->img);
		return (1);
	}
	return (0);
}

void	win_flush(t_win *win)
{
	mlx_put_image_to_window(win->mlx, win->win, win->img, 0, 0);
}

void	win_close(t_win *win)
{
	mlx_destroy_window(win->mlx, win->win);
	mlx_destroy_image(win->mlx, win->img);
}

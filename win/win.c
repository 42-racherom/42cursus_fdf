/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 01:25:23 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 04:30:04 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <point.h>
#include <string.h>
#include <win.h>

int	win_pixel_put(t_win *win, t_point_2d p)
{
	char	*dst;

	if (p.x < 0 || p.x >= win->w || p.y < 0 || p.y >= win->h)
		return (0);
	dst = win->addr + (((int)p.y * win->ll) + ((int)p.x * (win->bpp / 8)));
	*(unsigned int *)dst = p.c;
	return (0);
}

int	get_gradient_color(int start_color, int end_color, float ratio)
{
	int		r;
	int		g;
	int		b;
	float	nratio;

	if (ratio <= 0)
		return (start_color);
	if (ratio >= 1)
		return (end_color);
	nratio = 1 - ratio;
	r = ((start_color >> 16) & 0xFF) * nratio + ((end_color >> 16) & 0xFF)
		* ratio;
	g = ((start_color >> 8) & 0xFF) * nratio + ((end_color >> 8) & 0xFF)
		* ratio;
	b = (start_color & 0xFF) * nratio + (end_color & 0xFF) * ratio;
	return (r << 16 | g << 8 | b);
}

void	win_draw_line(t_win *win, t_bound_2d b)
{
	t_point_2d	d;
	t_point_2d	p;
	float		s;
	float		dist;

	d.x = b.max.x - b.min.x;
	d.y = b.max.y - b.min.y;
	s = fabsf(d.y);
	if (fabsf(d.x) >= s)
		s = fabsf(d.x);
	d.x /= s;
	d.y /= s;
	p = b.min;
	dist = distance_2d(b.min, b.max);
	while (in_bound_floor_2d(b, p))
	{
		p.c = get_gradient_color(b.min.c, b.max.c, distance_2d(b.min, p)
				/ dist);
		win_pixel_put(win, p);
		p.x += d.x;
		p.y += d.y;
	}
}

void	win_background(t_win *win, int color)
{
	int		i;
	int		max;
	char	*dst;

	i = 0;
	max = win->h * win->w;
	while (i < max)
	{
		dst = win->addr + (i++) * (win->bpp / 8);
		*(unsigned int *)dst = color;
	}
}

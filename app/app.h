/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 00:52:40 by rauer             #+#    #+#             */
/*   Updated: 2023/11/09 21:59:44 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef APP_H
# define APP_H
# include <fdf.h>
# include <point.h>
# include <win.h>

typedef struct s_app
{
	t_point_2d		mouse;
	int				r;
	float			zoom;
	unsigned int	keys;
	t_point_2d		off;
	t_fdf			fdf;
	t_win			win;
}					t_app;

typedef struct s_renderer
{
	int				i;
	float			r;
	t_bound_2d		b;
	t_point_2d		*row;
}					t_renderer;

int					app_parse(t_app *app, int argc, char **argv);
int					app_init(t_app *app);
int					app_start(t_app *app);
int					app_close(t_app *app);

#endif

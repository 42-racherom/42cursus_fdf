/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_render.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 00:19:27 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 03:59:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <ft.h>
#include <isometric.h>
#include <math.h>
#include <mlx.h>
#include <point.h>
#include <stdio.h>
#include <win.h>

t_bound_2d	project_bound(t_bound_3d b3d)
{
	t_bound_2d	b2d;
	t_point_3d	p3d;
	t_point_2d	o;

	o = empty_point_2d();
	p3d = b3d.min;
	b2d.min = isometric_project(p3d, o, 1);
	b2d.max = b2d.min;
	p3d.z = b3d.max.z;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	p3d.y = b3d.max.y;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	p3d.z = b3d.min.z;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	p3d.x = b3d.max.x;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	p3d.z = b3d.max.z;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	p3d.y = b3d.min.y;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	p3d.z = b3d.min.z;
	update_bound_2d(&b2d, isometric_project(p3d, o, 1));
	return (b2d);
}

void	app_calculate_zoom(t_app *app)
{
	t_point_2d	p;

	p = size_2d(project_bound(app->fdf.b));
	p.x = (app->win.w - 40) / (p.x);
	p.y = (app->win.h - 40) / (p.y);
	app->zoom = p.x;
	if (p.y < p.x)
		app->zoom = p.y;
}

void	app_calculate_offset(t_app *app)
{
	t_point_2d	cw;
	t_point_2d	cp;

	cw.x = app->win.w / 2;
	cw.y = app->win.h / 2;
	cp = isometric_project(app->fdf.c, empty_point_2d(), app->zoom);
	app->off = distance_point_2d(cp, cw);
}

void	app_render_point(t_app *app, t_renderer *r)
{
	int			x;
	t_point_3d	p;

	x = r->i % app->fdf.w;
	p = rotate_z(app->fdf.a[r->i], app->fdf.c, r->r);
	r->b.max = isometric_project(p, app->off, app->zoom);
	if (x > 0)
		win_draw_line(&app->win, r->b);
	r->b.min = r->row[x];
	if (r->i >= app->fdf.w)
		win_draw_line(&app->win, r->b);
	r->b.min = r->b.max;
	r->row[x] = r->b.max;
	r->i++;
}

int	app_render(t_app *app)
{
	t_renderer	r;

	r.i = 0;
	r.r = M_PI / 180 * app->r;
	r.row = ft_calloc(app->fdf.w, sizeof(t_point_2d));
	if (!r.row)
		return (1);
	win_background(&app->win, app->fdf.bg_color);
	app_calculate_offset(app);
	while (r.i < app->fdf.len)
		app_render_point(app, &r);
	free(r.row);
	win_flush(&app->win);
	return (0);
}

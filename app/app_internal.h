/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_internal.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 00:17:54 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 03:50:50 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef APP_INTERNAL_H
# define APP_INTERNAL_H
# include <fdf.h>
# include <ft.h>
# include <isometric.h>
# include <mlx.h>
# include <point.h>

typedef enum e_keyflag
{
	KEYFLAG_CONTROL = 1,
	KEYFLAG_W = 2,
	KEYFLAG_A = 4,
	KEYFLAG_S = 8,
	KEYFLAG_D = 16,
	KEYFLAG_MOUSE = 32
}		t_keyflag;

void	app_calculate_zoom(t_app *app);
void	app_calculate_offset(t_app *app);
void	app_project(t_app *app);
void	app_render(t_app *app);
int		app_hooks(t_app *app);

#endif

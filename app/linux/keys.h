/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 03:15:29 by racherom          #+#    #+#             */
/*   Updated: 2023/09/16 01:31:41 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H

typedef enum e_key
{
	KEY_CONTROL = 0xffe3,
	KEY_ESC = 65307,
	KEY_W = 0x0077,
	KEY_A = 0x0061,
	KEY_S = 0x0073,
	KEY_D = 0x0064,
	KEY_C = 0x0063
}	t_key;

typedef enum e_event
{
	EVENT_KeyPress = 2,
	EVENT_KeyRelease = 3,
	EVENT_MouseDown = 4,
	EVENT_MouseUp = 5,
	EVENT_MouseMove = 6,
	EVENT_DestroyNotify = 17,
	EVENT_ResizeRequest = 25
}	t_event;

#endif

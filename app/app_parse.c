/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_parse.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 00:22:57 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 03:51:52 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <fcntl.h>
#include <fdf.h>
#include <ft.h>
#include <point.h>

int	app_parse_args(t_app *app, char o, char *arg)
{
	if (o == 'w')
		app->win.w = ft_atoi(arg);
	else if (o == 'h')
		app->win.h = ft_atoi(arg);
	else if (o == 'd')
		app->fdf.dist = ft_atoi(arg);
	else if (o == 'c')
		app->fdf.color = ft_hex_atoi(arg);
	else if (o == 'b')
		app->fdf.bg_color = ft_hex_atoi(arg);
	else
		return (1);
	return (0);
}

int	app_print_usage(char *prog)
{
	ft_printf("usage: %s [-w width] [-h height] [-d distance] ", prog);
	ft_printf("[-c color] [-b bg_color] file\n");
	ft_printf("\tfile: fdf file to parse.\noptions:\n");
	ft_printf("\t-w width: window width\n\t-h height: window height.\n");
	ft_printf("\t-d distance: distance between fdf points.\n");
	ft_printf("\t-c color: color of the points, if not specified in the fdf\n");
	ft_printf("\t-b bg_color: color of the background.\n");
	return (1);
}

int	app_parse(t_app *app, int argc, char **argv)
{
	int	fd;
	int	r;
	int	i;

	r = 0;
	i = 1;
	while (i < argc && argv[i] && argv[i][0] == '-' && r == 0)
	{
		r = app_parse_args(app, argv[i][1], argv[2]);
		argv += 2;
	}
	if (r != 0 || i >= argc)
		return (app_print_usage(argv[0]));
	fd = open(argv[i], O_RDONLY);
	if (fd < 0)
	{
		ft_printf("Unable to open file %s\n", argv[i]);
		return (1);
	}
	r = fdf_parse(&app->fdf, fd);
	if (r > 0)
		ft_printf("Invalide fdf file.\n");
	return (r);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 00:52:34 by rauer             #+#    #+#             */
/*   Updated: 2023/11/12 03:51:07 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <app_internal.h>
#include <fdf.h>
#include <ft.h>
#include <isometric.h>
#include <math.h>
#include <point.h>
#include <win.h>

int	app_init(t_app *app)
{
	app->win.w = 1500;
	app->win.h = 900;
	app->keys = 0;
	app->r = 0;
	return (fdf_init(&app->fdf));
}

int	app_start(t_app *app)
{
	app_calculate_zoom(app);
	if (win_init(&app->win))
		return (1);
	app_hooks(app);
	app_render(app);
	return (mlx_loop(app->win.mlx));
}

int	app_close(t_app *app)
{
	win_close(&app->win);
	free(app->fdf.a);
	exit(0);
}

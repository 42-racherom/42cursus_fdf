/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_mouse_hooks.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/09 22:29:23 by racherom          #+#    #+#             */
/*   Updated: 2023/11/12 03:05:44 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <app_internal.h>
#include <keys.h>
#include <math.h>

int	app_update_center(t_app *app, t_point_2d off)
{
	t_point_3d	p;
	float		r;

	r = (M_PI / 180) * (360 - app->r);
	off.x += app->win.w / 2;
	off.y += app->win.h / 2;
	p = isometric_translate(off, app->off, app->zoom);
	app->fdf.c = rotate_z(p, app->fdf.c, r);
	return (0);
}

int	mouse_move_hook(int x, int y, t_app *app)
{
	t_point_2d	mouse;

	if (app->keys & KEYFLAG_MOUSE)
	{
		mouse.x = x;
		mouse.y = y;
		app_update_center(app, distance_point_2d(mouse, app->mouse));
		app->mouse = mouse;
	}
	return (0);
}

int	mouse_down_hook(int button, int x, int y, t_app *app)
{
	if (button == 1)
	{
		app->mouse.x = x;
		app->mouse.y = y;
		app->keys |= KEYFLAG_MOUSE;
	}
	else if (button == 4)
		app->zoom += 0.1;
	else if (button == 5)
		app->zoom -= 0.1;
	return (0);
}

int	mouse_up_hook(int button, int x, int y, t_app *app)
{
	mouse_move_hook(x, y, app);
	if (button == 1)
		app->keys &= ~KEYFLAG_MOUSE;
	return (0);
}

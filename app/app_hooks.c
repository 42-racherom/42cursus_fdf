/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_hooks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/03 23:59:51 by racherom          #+#    #+#             */
/*   Updated: 2023/11/13 23:09:47 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <app_internal.h>
#include <keys.h>
#include <math.h>
#include <mlx.h>

int	mouse_down_hook(int button, int x, int y, t_app *app);
int	mouse_move_hook(int x, int y, t_app *app);
int	mouse_up_hook(int button, int x, int y, t_app *app);

int	translate_key(int keycode)
{
	if (keycode == KEY_CONTROL)
		return (KEYFLAG_CONTROL);
	if (keycode == KEY_W)
		return (KEYFLAG_W);
	if (keycode == KEY_A)
		return (KEYFLAG_A);
	if (keycode == KEY_S)
		return (KEYFLAG_S);
	if (keycode == KEY_D)
		return (KEYFLAG_D);
	return (-1);
}

int	key_down_hook(int keycode, t_app *app)
{
	int	k;

	k = translate_key(keycode);
	if (k > 0)
		app->keys |= k;
	return (0);
}

int	key_up_hook(int keycode, t_app *app)
{
	int	k;

	k = translate_key(keycode);
	if (k > 0)
		app->keys &= ~k;
	else if (keycode == KEY_ESC)
		return (app_close(app));
	return (0);
}

int	loop_hook(t_app *app)
{
	int	mh;
	int	mv;

	mh = app->keys & (KEYFLAG_A | KEYFLAG_D);
	if (mh == KEYFLAG_A)
		app->r += 359;
	else if (mh == KEYFLAG_D)
		app->r++;
	mv = app->keys & (KEYFLAG_W | KEYFLAG_S);
	if (mv == KEYFLAG_W)
		app->zoom *= 1 / 0.99;
	else if (mv == KEYFLAG_S)
		app->zoom *= 0.99;
	app_render(app);
	return (0);
}

int	app_hooks(t_app *app)
{
	mlx_do_key_autorepeatoff(app->win.mlx);
	mlx_hook(app->win.win, EVENT_KeyPress, 1, key_down_hook, app);
	mlx_hook(app->win.win, EVENT_KeyRelease, 2, key_up_hook, app);
	mlx_hook(app->win.win, EVENT_MouseDown, 4, mouse_down_hook, app);
	mlx_hook(app->win.win, EVENT_MouseUp, 8, mouse_up_hook, app);
	mlx_hook(app->win.win, EVENT_MouseMove, 16, mouse_move_hook, app);
	mlx_hook(app->win.win, EVENT_DestroyNotify, 0, app_close, app);
	mlx_loop_hook(app->win.mlx, loop_hook, app);
	return (0);
}
